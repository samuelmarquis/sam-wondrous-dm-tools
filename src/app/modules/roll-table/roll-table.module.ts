import { RollTableViewComponent } from './roll-table-view.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RollTableRoutingModule } from './roll-table-routing.module';
import { TableComponent } from './table/table.component';

import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import { SingleTableComponent } from './table/single-table/single-table.component';

@NgModule({
  declarations: [
    RollTableViewComponent,
    TableComponent,
    SingleTableComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    RollTableRoutingModule
  ]
})
export class RollTableModule { }
