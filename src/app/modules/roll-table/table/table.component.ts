import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, OnDestroy {

  private subs$ : Subscription = new Subscription;
  private queryParam$ : Observable<any> = this.route.queryParams;

  tables : any;

  constructor(
    private httpClient: HttpClient,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.subs$.add(this.queryParam$.subscribe( ({jsonFile = null, tableName = []}) => {
      this.fetchData(jsonFile, tableName)
    }));
  }

  ngOnDestroy(): void {
    this.subs$.unsubscribe();
  }

  // tokenize = (prefix : string, table : string, row = "_") : string => `${prefix}_${table.replace(/[^a-zA-Z0-9-_]/g, "").toLowerCase()}_${row.replace(/[^a-zA-Z0-9-_]/g, "").toLowerCase()}`

  fetchData(jsonFile, tableName?){
    this.httpClient.get(`assets/json/${jsonFile}.json`).pipe(
      take(1)
    ).subscribe(data =>{
      console.log(data);
      this.tables = data;
    })
  }

  navigateToElement(id) : void {
    document.getElementById(id).scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest"
    });
  }
  scrollTop():void{
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }
}
