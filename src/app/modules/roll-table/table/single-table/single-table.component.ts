import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-single-table',
  templateUrl: './single-table.component.html',
  styleUrls: ['./single-table.component.scss']
})
export class SingleTableComponent implements OnInit {

  @Input() table;
  @Input() tableIndex;

  selectedRowData;
  lastRoll;

  constructor() { }

  ngOnInit(): void {
  }



  rollAndFind({dice, tableCol}, tableIndex){
    const className = "row-highLight";
    this.clearClass(className);
    const rollNumb = this.rollDice(1, dice);
    let rowIndex;

    tableCol.forEach((col, i)=> {
      const colRoll = col[0];
      const condition = colRoll.includes(rollNumb) || (rollNumb > colRoll[0] && rollNumb < colRoll[1]);
      if(condition) rowIndex = i;
    });
    this.selectedRowData = tableCol[rowIndex];
    this.lastRoll = rollNumb;
    console.log("this.selectedRowData",this.selectedRowData)
    const rowId = "table_" + tableIndex + "_row_" + rowIndex;
    document.getElementById(rowId).classList.add(className);
  }




  private rollDice(min, max) : number {
    return min + Math.floor(Math.random() * (max-min + 1))
  }

  private clearClass(className) : void {
    const elems : HTMLCollectionOf<Element> = document.getElementsByClassName(className);
    console.log('elems',elems)
    for(let i = 0 ; i<= elems.length ; i++){
      elems[i]?.classList?.remove(className);
    }
  }
}
