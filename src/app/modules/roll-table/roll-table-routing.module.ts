import { TableComponent } from './table/table.component';
import { RollTableViewComponent } from './roll-table-view.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';


const routes: Routes = [{
  path: '', component: RollTableViewComponent, children : [
    {path : "table", component : TableComponent}
  ]
},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RollTableRoutingModule { }
