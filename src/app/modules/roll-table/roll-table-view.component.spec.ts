import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RollTableViewComponent } from './roll-table-view.component';

describe('RollTableViewComponent', () => {
  let component: RollTableViewComponent;
  let fixture: ComponentFixture<RollTableViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RollTableViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RollTableViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
