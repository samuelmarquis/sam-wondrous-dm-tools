import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';

// const routes: Routes = [];

// const routerOptions: ExtraOptions = {
//   anchorScrolling: "enabled"
//   //scrollPositionRestoration: "enabled"
// };

const routes: Routes = [
  { path: 'roll-table', loadChildren: () => import('./modules/roll-table/roll-table.module').then(m => m.RollTableModule) },
  { path: '', redirectTo: 'roll-table', pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes,{ anchorScrolling: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
